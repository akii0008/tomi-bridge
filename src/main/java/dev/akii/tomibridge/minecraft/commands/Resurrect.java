package dev.akii.tomibridge.minecraft.commands;

import dev.akii.tomibridge.Reaper;
import dev.akii.tomibridge.TomiBridge;
import dev.akii.tomibridge.Utils;
import dev.akii.tomibridge.minecraft.CompositeCommand;
import dev.akii.tomibridge.minecraft.MCResponse;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Resurrect extends CompositeCommand {

  private final TomiBridge plugin;
  private final Reaper reaper;

  public Resurrect(TomiBridge plugin) {
    super("resurrect", "Resurrects a user by IP", "<command> <IP>", new ArrayList<>());
    this.plugin = plugin;
    this.reaper = plugin.getReaper();
  }

  @Override
  public void setup() {
    this.setPermission("tomibridge.resurrect");
  }

  @Override
  public boolean call(@NotNull CommandSender sender, @NotNull String[] args) {
    String IP;
    try {
      IP = args[0];
    } catch (ArrayIndexOutOfBoundsException e) {
      sender.sendMessage(MCResponse.error("Missing an IP to resurrect"));
      return true;
    }

    if (args[0].equals("(nobody to resurrect)")) {
      sender.sendMessage(MCResponse.info("There's nobody to resurrect."));
      return true;
    }

    Utils.CustomConfig reaperConfig = reaper.getConfig();

    String IPwUnderscores = IP.replaceAll("\\.", "_");
    String discordID = reaperConfig.getConfig().getString(IPwUnderscores + ".discordID");
    if (discordID == null) {
      sender.sendMessage(MCResponse.error("User does not exist to resurrect"));
      return true;
    }

    String guildID = plugin.getConfig().getString("discord.guildID");
    assert guildID != null;
    Guild guild = plugin.getBot().getAPI().getGuildById(guildID);
    assert guild != null;
    Member member = guild.retrieveMemberById(discordID).complete();

    reaper.resurrect(member, IPwUnderscores);

    sender.sendMessage(MCResponse.success("Resurrected " + member.getEffectiveName() + " (" + IP + ")"));

    return true;
  }

  @Override
  public @NotNull List<String> tabComplete(@NotNull CommandSender sender, @NotNull String alias, String[] args) {
    Set<String> IPs = reaper.getConfig().getConfig().getKeys(false);

    if (IPs.size() == 0) {
      ArrayList<String> zeroArray = new ArrayList<>();
      zeroArray.add("(nobody to resurrect)");
      return zeroArray;
    }

    return new ArrayList<>(IPs);
  }
}
