package dev.akii.tomibridge.discord.commands;

import dev.akii.tomibridge.Reaper;
import dev.akii.tomibridge.TomiBridge;
import dev.akii.tomibridge.Utils;
import dev.akii.tomibridge.discord.DiscordCommandInterface;
import dev.akii.tomibridge.discord.Lambot;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;

public class Resurrect implements DiscordCommandInterface {

  @Override
  public void run(TomiBridge plugin, Lambot bot, Message message, String[] args) {
    String IP;
    try {
      IP = args[0];
    } catch (ArrayIndexOutOfBoundsException e) {
      message.getChannel().sendMessage(":x: **Missing an IP to resurrect!**").queue();
      return;
    }

    Reaper reaper = plugin.getReaper();
    Utils.CustomConfig reaperConfig = reaper.getConfig();

    String IPwUnderscores = IP.replaceAll("\\.", "_");
    String discordID = reaperConfig.getConfig().getString(IPwUnderscores + ".discordID");
    if (discordID == null) {
      message.getChannel().sendMessage(":x: **User does not exist to resurrect.**").queue();
      return;
    }

    String guildID = plugin.getConfig().getString("discord.guildID");
    assert guildID != null;
    Guild guild = bot.getAPI().getGuildById(guildID);
    assert guild != null;
    Member member = guild.retrieveMemberById(discordID).complete();

    reaper.resurrect(member, IPwUnderscores);

    message.getChannel().sendMessage(":white_check_mark: **Resurrected `" + member.getEffectiveName() + "`** (`" + IP + "`)").queue();
  }
}
