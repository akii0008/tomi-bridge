package dev.akii.tomibridge.discord.commands;

import com.fasterxml.jackson.annotation.JsonProperty;
import dev.akii.tomibridge.TomiBridge;
import dev.akii.tomibridge.Utils;
import dev.akii.tomibridge.discord.DiscordCommandInterface;
import dev.akii.tomibridge.discord.Lambot;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.jetbrains.annotations.NotNull;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

public class Nepotism implements DiscordCommandInterface {

  public void run(TomiBridge plugin, Lambot bot, @NotNull Message message, String[] args) {
    plugin.getLogger().info(message.getAuthor().getId());

    String[] arr = {"812403974997147658", "480948873158721597"};
    if (Arrays.stream(arr).noneMatch(message.getAuthor().getId()::equals)) {
      message.getChannel().sendMessage(":x: **You do not have access to this command.**").queue();
      return;
    }

    String discordIDArg;
    try {
      discordIDArg = args[0];
    } catch (ArrayIndexOutOfBoundsException e) {
      message.getChannel().sendMessage(":x: **Missing Discord ID to whitelist.**").queue();
      return;
    }

    User userObj = bot.getAPI().retrieveUserById(discordIDArg).complete();
    if (userObj == null) {
      message.getChannel().sendMessage(":x: **User ID was invalid.**").queue();
      return;
    }

    String minecraftUsername;
    try {
      minecraftUsername = args[1];
    } catch (ArrayIndexOutOfBoundsException e) {
      message.getChannel().sendMessage(":x: **Missing Minecraft username to whitelist.**").queue();
      return;
    }

    plugin.getLogger().info(minecraftUsername);

    String urlToSend = "https://api.mojang.com/users/profiles/minecraft/" + minecraftUsername + "?at=" + System.currentTimeMillis() / 1000L;

    HttpClient client = HttpClient.newHttpClient();
    HttpRequest request = HttpRequest.newBuilder(
                    URI.create(urlToSend)
            )
            .header("accept", "application/json")
            .build();

    CompletableFuture<HttpResponse<Supplier<PlayerJSON>>> response = client.sendAsync(request, new Utils.JsonBodyHandler<>(PlayerJSON.class));

    message.getChannel().sendMessage(":gear: **Getting UUID...**").queue(msg -> {
      try {
        HttpResponse<Supplier<PlayerJSON>> res = response.get();

        String discordID = userObj.getId();
        String UUID = res.body().get().id;
        String formattedUUID = Utils.formatUUID(UUID);

        plugin.getLogger().fine(UUID);
        plugin.getLogger().fine(formattedUUID);

        Utils.CustomConfig whitelist = plugin.getWhitelist();

        String user = whitelist.getConfig().getString(formattedUUID);
        if (user != null) {
          msg.editMessage(":x: **They already have an account linked.**").queue();
          return;
        }

        whitelist.getConfig().addDefault(formattedUUID + ".type", "nepotism");
        whitelist.getConfig().addDefault(formattedUUID + ".discordID", discordID);
        whitelist.saveConfig();

        msg.editMessage(":white_check_mark: **Successfully added `" + minecraftUsername + "` (`" + formattedUUID + "`) to the whitelist.**").queue();
      } catch (Exception e) {
        msg.editMessage(":x: **Sorry! There has been some sort of error.** Please check the username spelling or try again later.").queue();
        e.printStackTrace();
      }
    });

  }

  // This class provides a source for deserialization the JSON we receive from Mojang
  private static class PlayerJSON {
    public final String name;
    public final String id;

    public PlayerJSON(
            @JsonProperty("name") String name,
            @JsonProperty("id") String id
    ) {
      this.name = name;
      this.id = id;
    }
  }

}
